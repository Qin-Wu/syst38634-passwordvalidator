package Validator;

/*
 * @author Qin Wu, 991520749
 * This class validates password and it will be developed using TDD
 */
public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS_NUM = 2;

	public static boolean isValidLength(String password) {
		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}
		return false;
	}

	public static boolean hasEnoughDigits(String password) {
		int numOfDigits = 0;

		if (password != null) {
			char[] ch = password.toCharArray();

			for (int i = 0; i < ch.length; i++) {
				if (ch[i] >= 48 && ch[i] <= 57) {
					numOfDigits++;
				}
			}
			return (numOfDigits >= MIN_DIGITS_NUM);
		}
		return false;
	}

}

