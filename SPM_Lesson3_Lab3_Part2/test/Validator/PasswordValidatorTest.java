package Validator;
/*
 * @author Qin Wu, 991520749
 * This class validates password and it will be developed using TDD
 */
import static org.junit.Assert.*;
import org.junit.Test;

public class PasswordValidatorTest {
	@Test
	public void testHasEnoughDigits() {
		assertTrue("Password does not have enough digits.",PasswordValidator.hasEnoughDigits("AAa8bcdefg1"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Password does not have enough digits.",PasswordValidator.hasEnoughDigits(null));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Password does not have enough digits.",PasswordValidator.hasEnoughDigits("abcdef12"));
	}
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Password does not have enough digits.",PasswordValidator.hasEnoughDigits("abcdefg2"));
	}
	@Test
	public void testIsValidLength() {
		assertTrue("Invalid password length",PasswordValidator.isValidLength("1234567890"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length",PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length",PasswordValidator.isValidLength("       "));
	}
	
	@Test
	public void testIsValidLengthBoundryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("76543210"));
	}
	
	@Test
	public void testIsValidLengthBoundryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("7654321"));		
	}
	
	
	
	
	
	
	

	
//	@Test
//	public void testIsValidLengthRegular() {
//		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
//
//	}
//
//	@Test
//	public void testIsValidLengthException() {
//		assertFalse("Invalid password length", PasswordValidator.isValidLength(null));
//	}
//	
//	@Test
//	public void testIsValidLengthExceptionSpaces() {
//		assertFalse("Invalid password length", PasswordValidator.isValidLength("       "));
//
//	}
//	
//	@Test
//	public void testIsValidLengthBoundaryIn() {
//		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
//
//	}

}
